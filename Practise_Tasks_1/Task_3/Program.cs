﻿using System;
using ThirdTask.Classes;

namespace ThirdTask
{
    class Program
    {
        static void Main(string[] args)
        {
            // Let's create some polynomial and print it.
            var array = new int[] { 3, 4, -2, 1 };
            var polynomial1 = new Polynomial(array);
            Console.WriteLine($"First polynomial: {polynomial1.ToString()}");

            // Let's calculate this polynomial by argument.
            Console.WriteLine($"Calculation with argument 5: {polynomial1.Calculate(5).ToString()}\n");

            // Changing a cofficient on certain position.
            polynomial1[2] = 2;
            Console.WriteLine($"Polynomial after changing: {polynomial1.ToString()}\n");

            // Let's create a new polynomial with bigger degree and try to use math operations.
            array = new int[] { 6, -3, 5, 2, -4 };
            var polynomial2 = new Polynomial(array);
            Console.WriteLine($"Second polynomial: {polynomial2.ToString()}\n");

            Console.WriteLine($"Addition: {(polynomial1+polynomial2).ToString()}");
            Console.WriteLine($"Subtraction: {(polynomial1 - polynomial2).ToString()}");
            Console.WriteLine($"Multiplication: {(polynomial1 * polynomial2).ToString()}");

            Console.ReadKey();
        }
    }
}